#!/bin/bash

export MYLATEXHOME=${PWD}
export BIBINPUTS=:${MYLATEXHOME}/biblio
export TEXINPUTS=:${MYLATEXHOME}/classes
export TEXINPUTS=${TEXINPUTS}:${MYLATEXHOME}/macros
export TEXMFHOME=${MYLATEXHOME}/texmf
export PATH=${PATH}:${MYLATEXHOME}/bin
