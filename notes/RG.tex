
\section{Ritz and Galerkin methods for elliptic problems}\label{sec:rg}

In Section \ref{sec:wf} we have reformulated the Dirichlet problem to seek weak solutions and we showed its well-posedness. The problem being infinite dimensional, it is not computable.
It should also be noted that computability is not the only reason to build a solution with the Galerkin method, but also to prove the existence of a solution by the method of approximate problems.

\medskip
\Question How can we construct an approximation to Problem \eqref{pb:poisson} which is also well-posed and how does the solution to this problem compare to the solution of the original problem ?

\subsection{Approximate problem}

In the previous section we showed how a classical PDE problem such as Problem \eqref{pb:poisson} can be reformulated as a weak problem. 
The abstract problem for this class of PDE reads then:
\begin{equation}\label{pb:rg_abstract}
\left\lvert
\begin{array}{ll}
\mbox{Find $u \in \xV$, such that:}\\[2ex]
\displaystyle a( u, v ) = L(v)\quad,\;\forany  v\in \xV
\end{array}
\right .
\end{equation}
with $a(\xDot,\xDot)$ a coercive continuous bilinear form on $\xV\times\xV$ and $L(\xDot)$ a continuous linear form on $\xV$. 

\medskip
Since in the case of the Poisson problem the bilinear form is continuous, coercive and symmetric, the well-posedness follows directly from Riesz--Fréchet representation Theorem. If the bilinear form is still coercive but not symmetric then we will see that the well-posedness is proven by the Lax--Milgram Theorem.

But for the moment, let us focus on the symmetric case: provided that the well-posedness holds, we want now to construct an approximate solution $u_n$ to the Problem \eqref{pb:rg_abstract}.

\subsection{Ritz method for symmetric bilinear forms}

\subsubsection{Formulation}

Ritz's method is based on replacing the solution space $\xV$ (which is infinite dimensional) by a finite dimensional subspace $\xV_n \subset \xV$, $\dim(\xV_n) = n$.

\medskip
Problem \eqref{pb:ritz_weak} is the approximate weak problem by Ritz's method:
\begin{equation}\label{pb:ritz_weak}
\left\lvert
\begin{array}{ll}
\mbox{Find $u_n \in \xV_n$, $\xV_n \subset \xV$, such that:}\\[2ex]
a( u_n, v_n ) = L(v_n)\quad,\forany  v_n \in \xV_n\\[2ex]
\end{array}
\right .
\end{equation}
with $a(\xDot,\xDot)$ a coercive symmetric continuous bilinear form on $\xV\times\xV$ and $L(\xDot)$ a continuous linear form on $\xV$. 

\medskip
Provided that the bilinear form is symmetric, Problem \eqref{pb:ritz_variational} is the equivalent approximate variational problem under minimisation form:
\begin{equation}\label{pb:ritz_variational}
\left\lvert
\begin{array}{ll}
\mbox{Find $u_n \in \xV_n$, $\xV_n \subset \xV$, such that:}\\[2ex]
J(u_n) \leq J(v_n)\quad,\forany  v_n \in \xV_n\\[2ex]
\mbox{with $J(v_n) = \frac{1}{2} a(v_n,v_n) - L(v_n)$}
\end{array}
\right .
\end{equation}

Solution
\begin{equation}
u_n = \sum_{j = 1}^{n} u_j \varphi_j
\end{equation}
where ${u_j}_{1\leq j \leq n}$ is a family of real numbers and $\Basis= (\varphi_1,\dots,\varphi_n)$ a basis of $\xV_n$.

\subsubsection{Well-posedness}

\medskip
\begin{thrm}[Well-posedness]
Let $\xV$ be a Hilbert space and $\xV_n$ a finite dimensional subspace of $\xV$, $\dim(\xV_n) = n$, Problem \eqref{pb:ritz_weak} admits a unique solution $u_n$.
\end{thrm}

\begin{proof}
The proof can either use directly the Lax--Milgram Theorem or show that there exists a unique solution to the equivalent minimisation problem \eqref{pb:ritz_variational} by explicitly constructing an approximation $u_n \in \xV_n$ decomposed on a basis $(\varphi_1,\cdots,\varphi_n)$ of $\xV_n$:
\begin{equation*}
u_n = \sum_{j=1}^{n} u_j\; \varphi_j
\end{equation*}
In so doing, the constructive approach paves the way to the Finite Element Method and is thus chosen as a prequel to establishing the Galerkin method.

\medskip
Writing the minimisation functional for $u_n$ reads:
\begin{eqnarray*}
J(u_n) &=& \frac{1}{2}\,a(u_n,u_n) - L(u_n) \\
       &=& \frac{1}{2}\,a(\sum_{j = 1}^{n} u_j \varphi_j,\sum_{i = 1}^{n} u_i \varphi_i) - L(\sum_{j = 1}^{n} u_i \varphi_i) \\
       &=& \frac{1}{2}\,\sum_{i = 1}^{n} \sum_{j = 1}^{n} a(u_j \varphi_j,u_i \varphi_i) - \sum_{j = 1}^{n}  L(u_i \varphi_i) \\
       &=& \frac{1}{2}\,\sum_{i = 1}^{n} \sum_{j = 1}^{n} u_j u_i a(\varphi_j, \varphi_i) - \sum_{j = 1}^{n} u_i L(\varphi_i)
\end{eqnarray*}

\medskip
Collecting the entries by index $i$, the functional can be rewritten under algebraic form:
\begin{equation*}
\matJ(\vecu) = \frac{1}{2} \vecu^t \matA \vecu - \vecu^t \vecb
\end{equation*}
where $\vecu$ is the unknow vector:
\begin{equation*}
\vecu^t = (u_1,\dots,u_n)
\end{equation*}
and $\matA$, $\vecb$ are respectively the stiffnes matrix and load vector:
\begin{equation*}
\matA_{ij} = a(\varphi_j, \varphi_i), \vecb_{i} = L(\varphi_i)
\end{equation*}

Owing to Proposition \ref{prp:convexity_J},$\matJ$ is a strictly convex quadratic form, then there exists a unique  $\vecu \in \xR^n\;:\:\matJ(\vecu) \leq \matJ(\vecv), \forany  \vecv \in \xR^n$, which in turns proves the existence and uniqueness of $u_n \in \xV_n$.

\medskip
The minimum is achieved with $\vecu$ satisfying $\matA \vecu = \vecb$ which corresponds to the Euler condition $J'(u_n) = 0$
\end{proof}


\begin{prpstn}[Convexity of a quadratic form]\label{prp:convexity_J}
\begin{equation*}
\matJ(\vecu) = \vecu^t \matK \vecu - \vecu^t \matG + \matF
\end{equation*}
is a strictly convex quadratic functional iff $\matK$ symmetric positive definite non-singular.
\end{prpstn}

\subsubsection{Convergence}

The question in this section is: considering a sequence of discrete solutions $\left(u_n\right)_{n\in\xN}$, with each $u_n$ belonging to $\xV_n$, can we prove that $u_n \tendsto u$ in $\xV$ as $n \tendsto \infty$ ?

\begin{lmm}[Estimate in the energy norm]\label{lm:energy_norm}
Let $\xV$ be a Hilbert space and $\xV_n$ a finite dimensional subspace of $\xV$.
We denote by $u \in \xV$, $u_n \in \xV_n$ respectively the solution to Problem \eqref{pb:rg_abstract} and the solution to approximate Problem \eqref{pb:ritz_weak}.
Let us define the energy norm $\norm{\xDot}_a = a(\xDot,\xDot)^{1/2}$, then the following inequality holds:
\begin{equation*}
\norm{u - u_n}_a  \leq \norm{u - v_n}_a\quad,\; \forany v_n \in \xV_n
\end{equation*}
\end{lmm}

\begin{proof}
Using the coercivity and the continuity of the bilinear form, we have:
\begin{equation*}
\alpha \norm{u}_\xV^2 \leq \norm{u}^2_a \leq \norm{u}^2_\xV
\end{equation*}
then $\norm{u}_a$ is norm equivalent to $\norm{u}_\xV$, thus $(\xV,\norm{\xDot}_a)$ is a Hilbert space.
\begin{equation*}
a( u - \Proj{\xV_n} u, v_n ) = 0\quad,\forany  v_n \in \xV_n
\end{equation*}
by definition of $\Proj{\xV_n}$ as the orthogonal projection of $u$ onto $\xV_n$ with respect to the scalar product defined by the bilinear form $a$.
\begin{equation*}
\norm{u - u_n}_a^2 = a(u-u_n, u - v_n) + a(u - u_n, v_n - u_n)\quad,\forany  v_n \in \xV_n
\end{equation*}
Since the second term of the right-hand side cancels due to the consistency of the approximation, we deduce $u_n = \Proj{\xV_n} u$, then $u_n$ minimizes the distance from $u$ to $\xV_n$:
\begin{equation*}
\norm{u - u_n}_a^2 \leq \norm{u - v_n}_a^2\quad,\forany  v_n \in \xV_n
\end{equation*}
which means that the error estimate is \textit{optimal} in the energy norm.
\end{proof}

\begin{lmm}[Céa's Lemma]\label{lm:cea}
Let $\xV$ be a Hilbert space and $\xV_n$ a finite dimensional subspace of $\xV$.
we denote by $u \in \xV$, $u_n \in \xV_n$ respectively the solution to Problem \eqref{pb:rg_abstract} and the solution to approximate Problem \eqref{pb:ritz_weak}
, then the following inequality holds:
\begin{equation*}
\norm{u - u_n}_\xV  \leq \sqrt{\dfrac{M}{\alpha}} \norm{u - v_n}_\xV\quad,\; \forany v_n \in \xV_n
\end{equation*}
with $M > 0$ the continuity constant and $\alpha > 0$ the coercivity constant.
\end{lmm}
\begin{proof}
Using the coercivity and continuity of the bilinear form, we bound the left-hand side of the estimate \eqref{lm:energy_norm} from below and its right-hand side from above:
\begin{equation*}
\alpha \norm{u - u_n}_\xV^2 \leq M \norm{u - v_n}_\xV^2\quad \,\forany  v_n \in \xV_n
\end{equation*}
Consequently:
\begin{equation*}
\norm{u - u_n}_\xV \leq \sqrt{\dfrac{M}{\alpha}} \norm{u - v_n}_\xV\quad,\; \forany v_n \in \xV_n
\end{equation*}
\end{proof}

Lemma \eqref{lm:cea} gives a control on the discretisation error $e_n = u - u_n$ which is \textit{quasi-optimal} in the $\xV$-norm (\ie bound multiplied by a constant).

\begin{lmm}[Stability]\label{lm:stability_elliptic}
Any solution $u_n \in \xV_n$ to Problem \eqref{pb:ritz_weak} satisfies:
\begin{equation*}
\norm{u_n}_\xV  \leq \dfrac{\norm{L}_{\xV'}}{\alpha}
\end{equation*}
\end{lmm}
\begin{proof}
Direct using the coercivity and the dual norm.
\end{proof}

\subsubsection{Method}

\begin{lgrthm}[Ritz's method]\label{alg:ritz} The following procedure applies:
\begin{enumerate}
\item Chose an approximation space $\xV_n$
\item Construct a basis $\Basis= (\varphi_1,\dots,\varphi_n)$
\item Assemble stiffness matrix $\matA$ and load vector $\vecb$
\item Solve $\matA \vecu = \vecb$ as a minimisation problem
\end{enumerate}
\end{lgrthm}

\subsection{Galerkin method}

\subsubsection{Formulation}

We use a similar approach as for Ritz's method, except that the abstract problem does not require the symmetry of the bilinear form.
Therefore we cannot endow $\xV$ with a norm defined from the scalar product based on $a(\xDot,\xDot)$.

\medskip
Problem \eqref{pb:galerkin_weak} is the approximate weak problem by Galerkin's method:
\begin{equation}\label{pb:galerkin_weak}
\left\lvert
\begin{array}{ll}
\mbox{Find $u_n \in \xV_n$, $\xV_n \subset \xV$, such that:}\\[2ex]
a( u_n, v_n ) = L(v_n)\quad,\forany  v_n \in \xV_n\\[2ex]
\end{array}
\right .
\end{equation}
with $a(\xDot,\xDot)$ a coercive continuous bilinear form on $\xV\times\xV$ and $L(\xDot)$ a continuous linear form on $\xV$. 


\subsubsection{Convergence}

The following property is merely a consequence of the consistency, as the continuous solution $u$ is solution to the discrete problem (\ie the bilinear form is the ``same''), but it is quite useful to derive error estimates in Section \ref{sec:error_analysis}.
Consequently, whenever needed we will refer to the following proposition:
\begin{prpstn}[Galerkin orthogonality]
 Let $u \in \xV$, $u_n \in \xV_n$ respectively the solution to Problem \eqref{pb:rg_abstract} and the solution to approximate Problem \eqref{pb:galerkin_weak}, then:
\begin{equation*}
a(\;u - u_n, v_n\;) = 0\quad,\;\forany v_n \in \xV_n
\end{equation*}
\end{prpstn}
\begin{proof}
Direct consequence of the consistency of the method.
\end{proof}

\begin{lmm}[Consistency]
Let $\xV$ be a Hilbert space and $\xV_n$ a finite dimensional subspace of $\xV$.
we denote by $u \in \xV$, $u_n \in \xV_n$ respectively the solution to Problem \eqref{pb:rg_abstract} and the solution to approximate Problem \eqref{pb:galerkin_weak}, then the following inequality holds:
\begin{equation*}
\norm{u - u_n}_\xV  \leq \dfrac{M}{\alpha} \norm{u - v_n}_\xV\quad,\; \forany v_n \in \xV_n
\end{equation*}
with $M > 0$ the continuity constant and $\alpha > 0$ the coercivity constant.
\end{lmm}
\begin{proof}
Using the coercivity:
\begin{eqnarray*}
\alpha \norm{u - u_n}_\xV^2 &\leq& a(u - u_n, u - u_n)\\
 &\leq& a(u - u_n, u - v_n) + \underbrace{a(u - u_n, v_n - u_n)}_{0}\\
 &\leq& a(u - u_n, u - v_n)\\
 &\leq& M \norm{u - u_n}_\xV \norm{u - v_n}_\xV\\
\norm{u - u_n}_\xV &\leq& \dfrac{M}{\alpha} \norm{u - v_n}_\xV\\
\end{eqnarray*}
\end{proof}

The only difference with the symmetric case is that the constant is squared due to the loss of the symmetry.

\subsubsection{Well-posedness}

\begin{thrm}[Lax--Milgram]
Let $\xV$ be a Hilbert space.
Provided that $a(\xDot,\xDot)$ is a coercive continuous bilinear form on $\xV\times\xV$ and $L(\xDot)$ is a continuous linear form on $\xV$, Problem \eqref{pb:abstract} admits a unique solution $u \in \xV$.
\end{thrm}
\begin{proof}
\end{proof}

\subsubsection{Method}

\begin{lgrthm}[Galerkin's method]\label{alg:galerkin} The following procedure applies:
\begin{enumerate}
\item Chose an approximation space $\xV_n$
\item Construct a basis $\Basis= (\varphi_1,\dots,\varphi_n)$
\item Assemble stiffness matrix $\matA$ and load vector $\vecb$
\item Solve $\matA \vecu = \vecb$
\end{enumerate}
\end{lgrthm}

%\subsection{A note on the Petrov--Galerkin method}

\subsection{Exercises}

\begin{xrcs}[Ritz Galerkin method --- 1]
Let us consider Problem \eqref{ex:poisson} from the previous chapter:
\begin{enumerate}
\item Formulate the approximation to this problem using the (finite dimensional) space of continuous functions, piecewise linear on intervals
\begin{equation*}
\Set{ \left[ 0,\frac 1 4 \right],\; \left[\frac 1 4,\; \frac 1 2 \right],\; \left[\frac 1 2, \frac 3 4 \right],\; \left[\frac 3 4, 1 \right]}
\end{equation*}
\item Write the basis functions.
\item Construct the linear system explicitely.
\end{enumerate}
\end{xrcs}

\begin{xrcs}[Ritz Galerkin method --- 2]
Repeat the previous exercise with boundary condition $u(1) = 2$.
\end{xrcs}

\begin{xrcs}[Lax Milgram]
Consider the following problem posed on $\Omega \subset \mathbb{R}^2$:
\begin{equation}\label{ex:diff_reac_lm}
\left\lvert
\begin{array}{rrll}
\mbox{Find $u\in C^2(\bar{\Omega})$ such that:}\\[2ex]
 -\Div(k(\x)\xDot\Grad u(\x)) + r(\x)u &=& f(\x)  &,\forany \x \in \Omega \\
 u &=& 0 &,\forany  \x \in \partial\Omega
\end{array}
\right.
\end{equation}
with the source term $f \in L^2(\Omega)$, the diffusive coefficient $0 < \alpha \leq k(x) \leq \beta$, and the reaction coefficient $0 < \alpha \leq r(x) \leq \beta$.
\begin{enumerate}
\item Formulate the weak problem. 
\item Show that assumptions of the Lax-Milgram theorem hold.
\end{enumerate}
\end{xrcs}

