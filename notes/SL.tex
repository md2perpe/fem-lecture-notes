
\newpage
%-------------------------------------------------------------------------------
\section{Simplicial Lagrange Finite Element}\label{sec:lagrange}

\subsection{Polynomial interpolation in one dimension}

Let $\xP^k([a,b])$ be the space of polynomials $p = \sum_{i=0}^k \alpha_i x^i$ of degre lower or equal to $k$ on the interval $[a,b]$, with $c_i x^i$ the monomial of order $i$, $c_i$ a real number. 

\medskip
A natural basis of $\xP^k([a,b])$ consists of the set of monomials $\Set{1,x,x^2,\cdots,x^k}$.
We can verify that its elements are linearly independent.
But in the frame of Finite Elements we can chose another basis which is the Lagrange basis $\Set{\Lgr{k}{i}}_{0 \leq i \leq k}$ of degree $k$ defined on a set of $k+1$ points $\Fam{\xi_i}_{0 \leq i \leq k}$ which are called \textit{Lagrange nodes}.

\begin{dfntn}[Lagrange polynomials -- \cite{EG} page 21, \cite{CDE} page 76]
\label{def:lagrange_poly}
The Lagrange polynomial of degree $k$ associated with node $\xi_m$ reads:
\begin{equation*}
\Lgr{k}{m}(x) = \dfrac{\prod_{\substack{i=0\\i\not=m}}^k (x - \xi_i)}{\prod_{\substack{i=0\\i\not=m}}^k (\xi_m - \xi_i)}
\end{equation*}
\end{dfntn}

\begin{prpstn}[Nodal basis -- \cite{EG}]
Lagrange polynomials form a nodal basis \ie
\begin{equation*}
\Lgr{k}{i}(\xi_j) = \delta_{ij}\quad,\;0 \leq i,j \leq k
\end{equation*}
\end{prpstn}

\bigskip
The following result gives a pointwise control of the interpolation error:
\begin{thrm}[Pointwise interpolation inequality -- \cite{CDE} page 79]
\label{th:polyinterpol}
Let $u\in\xC^{k+1}([a,b])$ and $\Projh{k} u \in \xP^k([a,b])$ its Lagrange interpolate of order $k$, with Lagrange nodes $\Fam{\xi_i}_{0\leq i \leq k}$, then $\forany x \in [a,b]$:
\begin{equation*}
\Abs{u(x) - \Projh{k} u(x)} \leq \Abs{\dfrac{\prod_{i=0}^k (x - \xi_i)}{(k+1)!}}\max_{[a,b]}\Abs{\partial^{k+1} u}
\end{equation*}
\end{thrm}

\subsection{A nodal element}

Let us take $\Fam{\xi_1, \cdots, \xi_N}$ a family of points of $K$ such that $\sigma_i(p)= p(\xi_i)$, $1\leq i \leq N$:
\begin{itemize}
\item $\Fam{\xi}_{1\leq i \leq N}$ is the set of \textit{geometrical nodes},
\item $\Fam{\varphi_i}_{1\leq i \leq N}$ is a \textit{nodal basis} of $\SpaceP$, \ie $\varphi_i(\xi_j) = \delta_{ij}$.
\end{itemize}
We can verify, for any $p \in\SpaceP$ that:
\begin{equation*}
p(\xi_j) = \sum_{i=1}^{N} \sigma_i(p)\; \underbrace{\varphi_i(\xi_i)}_{\delta_{ij}}\quad,\; 1 \leq i,j \leq N
\end{equation*}
which reduces to:
\begin{equation*}
p(\xi_j) = \sigma_i(p)
\end{equation*}

\medskip
\begin{rmrk}[Support of shape functions]
The polynomial basis being defined such that
\begin{equation*}
\begin{cases}
\varphi_i(\xi_i) = 1 \\
\varphi_i(\xi_j) = 0\;,\;i\not=j
\end{cases}
\end{equation*}
then any shape function $\varphi_i$ is compactly supported on the union of cells containing the node $\xi_i$.
\end{rmrk}

The Lagrange polynomials \eqref{def:lagrange_poly} are used to build directly the one-dimensional shape functions, while in higher dimensions the expression of the shape functions is reformulated in term of barycentric coordinates:
\begin{eqnarray*}
\lambda_i:& \xR^d &\fromto \xR\\
          & \bfx  &\mapsto \lambda_i(\bfx) = 1 - \dfrac{(\bfx - \mathbf\xi_i)\cdot\n_i}{(\mathbf\xi_{f} - \mathbf\xi_i)\cdot\n_i}
\end{eqnarray*}
with $\n_i$ the unit outward normal to the facet opposite to $\xi_i$, and $\xi_f$ a node belonging to this facet.

\medskip
\begin{xmpl}[Lagrange elements of polynomial degree $k=1,2$ on triangle]
The shape functions are given by:
\begin{equation*}
\begin{array}{lrlll}
k = 1, & \varphi_i &=& \lambda_i &,\;0 \leq i \leq d\\[2ex]
k = 2, & \varphi_i &=& \lambda_i (2 \lambda_i - 1) & ,\;0 \leq i \leq d\\
         & \varphi_i &=& 4 \lambda_i \lambda_j       & ,\;0 \leq i \leq d
\end{array}
\end{equation*}
\end{xmpl}

\subsection{Reference Finite Element}

\subsubsection{Examples in one dimension}

\begin{center}
\begin{figure}[h!]
\begin{tikzpicture}[y=4.0cm, x=4.0cm,font=\sffamily]
 	\coordinate (y) at (0,1.1);
    \coordinate (x) at (1.1,0);
    \draw[<->] (y) node[above] {$y$} -- (0,0) --  (x) node[right]
    {$x$};
	\draw[domain=0:1,smooth,variable=\x,blue] plot ({\x},{\x});
	\draw[domain=0:1,smooth,variable=\x,blue] plot ({\x},{1.0-\x});
\end{tikzpicture}
\caption{Lagrange $\xP^1$ on the unit interval.}
\end{figure}
\begin{figure}[h!]
\begin{tikzpicture}[y=4.0cm, x=4.0cm,font=\sffamily]
 	\coordinate (y) at (0,1.1);
    \coordinate (x) at (1.1,0);
    \draw[<->] (y) node[above] {$y$} -- (0,0) --  (x) node[right]
    {$x$};
	\draw[domain=0:1,smooth,variable=\x,blue] plot ({\x},{\x*(\x - 0.5)*2.0});
	\draw[domain=0:1,smooth,variable=\x,blue] plot ({\x},{(\x - 1.0)*(\x - 0.5)*2.0});
	\draw[domain=0:1,smooth,variable=\x,blue] plot ({\x},{(1.0 - \x)*\x*4.0});
\end{tikzpicture}
\caption{Lagrange $\xP^2$ on the unit interval.}
\end{figure}
\end{center}

\subsection{Formulation of the Poisson problem}

The approximate problem of Problem \eqref{pb:weak_poissonHone} by Lagrange $\xP^1$ elements reads:
\begin{subequations}\label{pb:poisson_p1}
\begin{equation}
\left\lvert
\begin{array}{ll}
\mbox{Find $u \in \xVh$, given $f \in \xLtwo(\dom)$, such that:}\\[2ex]
\displaystyle\int_\dom \Grad u\xDot \Grad v\dx = \int_\dom f v  \dx\quad,\;\forany  v\in \xVh
\end{array}
\right .
\end{equation}
with the approximation space $\xVh$ chosen as:
\begin{equation}
\xVh = \Set{ v \in \xCzero(\dom)\cap\xHonec(\dom) : v|_K \in\xP^1(K),\forany K \in\meshT }
\end{equation}
\end{subequations}

\subsection{Exercises}
